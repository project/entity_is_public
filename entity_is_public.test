<?php

class EntityIsPublicTestCase extends DrupalWebTestCase {

  protected $profile = 'testing';

  public static function getInfo() {
    return array(
      'name' => 'Functional tests',
      'description' => 'Tests for the FieldHelper class.',
      'group' => 'Entity is Public',
    );
  }

  public function setUp() {
    parent::setUp(array('entity', 'entity_is_public', 'path'));
  }

  public function testNodeIsPublic() {
    $node_type = $this->drupalCreateContentType();

    $node = $this->drupalCreateNode(array('type' => $node_type->type, 'status' => 1));
    $this->assertTrue(entity_is_public('node', $node));

    $this->assertFalse(entity_is_public('node', $node, array(), array('uri' => array('path' => ''))));
    $this->assertFalse(entity_is_public('node', $node, array(), array('uri' => array('path' => 'admin/node'))));
    variable_set('site_frontpage', 'admin/node');
    $this->assertTrue(entity_is_public('node', $node, array(), array('uri' => array('path' => 'admin/node'))));

    // Test the 'alias required' option.
    $this->assertFalse(entity_is_public('node', $node, array('alias required' => TRUE), array()));
    $node->path['alias'] = 'test-alias';
    node_save($node);
    drupal_static_reset();
    $this->assertTrue(entity_is_public('node', $node, array('alias required' => TRUE), array()));

    // Test unpublishing the node.
    $node->status = 0;
    node_save($node);
    drupal_static_reset();
    $this->assertFalse(entity_is_public('node', $node));
  }
}

class RabbitHoleEntityIsPublicTestCase extends DrupalWebTestCase {

  protected $profile = 'testing';

  public static function getInfo() {
    return array(
      'name' => 'Rabbit hole integration',
      'description' => 'Tests for the FieldHelper class.',
      'group' => 'Entity is Public',
    );
  }

  public function setUp() {
    parent::setUp(array('entity', 'entity_is_public', 'rabbit_hole', 'rh_node'));
  }

  public function testNodeIsPublic() {
    $node_type = $this->drupalCreateContentType();
    $node = $this->drupalCreateNode(array('type' => $node_type->type, 'status' => 1));
    $this->assertTrue(entity_is_public('node', $node));

    variable_set('rh_node_action_' . $node_type->type, RABBIT_HOLE_ACCESS_DENIED);
    $this->assertFalse(entity_is_public('node', $node));

    variable_set('rh_node_action_' . $node_type->type, RABBIT_HOLE_PAGE_NOT_FOUND);
    $this->assertFalse(entity_is_public('node', $node));

    variable_set('rh_node_action_' . $node_type->type, RABBIT_HOLE_PAGE_REDIRECT);
    $this->assertFalse(entity_is_public('node', $node));

    variable_set('rh_node_action_' . $node_type->type, RABBIT_HOLE_DISPLAY_CONTENT);
    $this->assertTrue(entity_is_public('node', $node));

    // Allow individual node overrides.
    variable_set('rh_node_override_' . $node_type->type, TRUE);

    $node->rh_action = RABBIT_HOLE_USE_DEFAULT;
    node_save($node);
    $this->assertTrue(entity_is_public('node', $node));

    $node->rh_action = RABBIT_HOLE_ACCESS_DENIED;
    node_save($node);
    $this->assertFalse(entity_is_public('node', $node));

    $node->rh_action = RABBIT_HOLE_PAGE_NOT_FOUND;
    node_save($node);
    $this->assertFalse(entity_is_public('node', $node));

    $node->rh_action = RABBIT_HOLE_PAGE_REDIRECT;
    node_save($node);
    $this->assertFalse(entity_is_public('node', $node));

    $node->rh_action = RABBIT_HOLE_DISPLAY_CONTENT;
    node_save($node);
    $this->assertTrue(entity_is_public('node', $node));
  }

}
